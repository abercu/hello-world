FROM openjdk:8-jdk-alpine
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} hello-world.jar
ENTRYPOINT ["java","-jar","/hello-world.jar"]