package com.abercu.training.helloworld.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@RestController
@RequestMapping("/hello")
public class HelloWorldController {

    @GetMapping
    public String helloWorld() {
        log.info("helloWorld()");

        return "Hello, World!";
    }

    @GetMapping("/from")
    public String helloFrom() throws UnknownHostException {
        log.info("helloFrom() - {} / {}",
                InetAddress.getLocalHost().getHostName(),
                InetAddress.getLocalHost().getHostAddress());

        return "Hello from: "
                + InetAddress.getLocalHost().getHostName()
                + " / "
                + InetAddress.getLocalHost().getHostAddress();
    }

    @GetMapping("/from-name")
    public String helloFromHostName() throws UnknownHostException {
        log.info("helloFromHostName() - {}", InetAddress.getLocalHost().getHostName());

        return "Hello from host name: " + InetAddress.getLocalHost().getHostName();
    }

    @GetMapping("/from-address")
    public String helloFromHostAddress() throws UnknownHostException {
        log.info("helloFromHostAddress() - {}", InetAddress.getLocalHost().getHostAddress());

        return "Hello from host address: " + InetAddress.getLocalHost().getHostAddress();
    }
}
